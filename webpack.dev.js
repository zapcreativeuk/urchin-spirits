const path = require("path")
const common = require("./webpack.common")
const { merge } = require("webpack-merge")
const { WebpackPluginServe: Serve } = require('webpack-plugin-serve');

module.exports = merge(common, {
  mode: "development",
  entry: [
    'webpack-plugin-serve/client',
    './inc/js/main.js'
  ],
  plugins: [
    new Serve({
      liveReload: true,
      port: 6969,
      static: 'dist',
    })
  ],
  watch: true
})
