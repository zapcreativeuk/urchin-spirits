const glob                  = require('glob');
const path                  = require('path');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');
const HTMLWebpackPlugin     = require('html-webpack-plugin');

const HTMLWebpackPluginFactory = () => glob.sync('./inc/**/*.html').map(
  dir => new HTMLWebpackPlugin({
    filename: path.basename(dir), // Output
    template: dir, // Input
  }),
);

module.exports = {
  entry: './inc/js/main.js',
  devtool: 'source-map',
  output: {
    path: `${__dirname}/dist`,
    filename: 'main.js',
    assetModuleFilename: 'assets/[hash][ext][query]'
  },
  module: {
    rules: [
      {
        test: /\.(html)$/,
        use: ['html-loader']
      },
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          //'style-loader',
          { loader: 'css-loader', options: { sourceMap: true, importLoaders: 1 } },
          { loader: 'sass-loader', options: { sourceMap: true } },
        ],
      },

      // Images: Copy image files to build folder
      { 
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i, 
        type: 'asset/resource',
      },

      // Fonts and SVGs: Inline files
      {
        test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, 
        type: 'asset/resource', 
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ 
      filename: './css/[name].css'
    }),
    ...HTMLWebpackPluginFactory()
  ],
  watch: false
}
